﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class gameover : MonoBehaviour
{
    GameObject LifeGauge;
    GameObject LifeGauge2;
    // Start is called before the first frame update
    void Start()
    {
        this.LifeGauge = GameObject.Find("HP1");
        this.LifeGauge2 = GameObject.Find("HP2");
    }

    // Update is called once per frame
    void Update()
    {
        if (this.LifeGauge.GetComponent < Image > ().fillAmount < 0.01f) 
        {
            SceneManager.LoadScene("gameoverScene");
        }
        if(this.LifeGauge2.GetComponent<Image>().fillAmount<0.01f)
        {
            SceneManager.LoadScene("1Pwin");
        }
    }
}
