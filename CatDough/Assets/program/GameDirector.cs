﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameDirector : MonoBehaviour
{
    GameObject HP1;
    GameObject HP2;

    // Start is called before the first frame update
    void Start()
    {
        this.HP1 = GameObject.Find("HP1");
        this.HP2 = GameObject.Find("HP2");
    }
    

    // Update is called once per frame
    void Update()
    {
        
    }
   public void DecreaseHp1()
    {
        this.HP1.GetComponent<Image>().fillAmount -= 0.1f;
    }
    public void DecreaseHp2()
    {
        this.HP2.GetComponent<Image>().fillAmount -= 0.1f;
    }
    public void DecreaseHp3()
    {
        this.HP1.GetComponent<Image>().fillAmount += 0.1f;
    }
    public void DecreaseHp4()
    {
        this.HP2.GetComponent<Image>().fillAmount += 0.1f;
    }
}
